import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'ui/add/add_binding.dart';
import 'ui/add/add_screen.dart';
import 'ui/edit/edit_binding.dart';
import 'ui/edit/edit_screen.dart';
import 'ui/home/home_binding.dart';
import 'ui/home/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized;
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Airsoft',
      theme: ThemeData(
        backgroundColor: Colors.blue.shade800,
      ),
      getPages: [
        GetPage(
          name: '/',
          page: () => const Homescreen(),
          binding: Homebinding(),
        ),
        GetPage(
          name: '/add',
          page: () => const Addscreen(),
          binding: Addbinding(),
        ),
        GetPage(
          name: '/edit',
          page: () => const Editscreen(),
          binding: Editbindng(),
        ),
      ],
      initialRoute: '/',
      // initialBinding: GlobalBinding(),
    );
  }
}
