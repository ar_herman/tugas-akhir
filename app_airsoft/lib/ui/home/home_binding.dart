import 'package:get/get.dart';

import 'home_controll.dart';

class Homebinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Homecontroller());
  }
}
