import 'package:app_airsoft/ui/home/home_controll.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../global/server.dart';

class Homescreen extends StatelessWidget {
  const Homescreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Homecontroller controller = Get.find<Homecontroller>();

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => Get.toNamed('/add'),
        child: const Icon(Icons.add),
      ),
      appBar: AppBar(
        title: const Text("Airsoft App"),
      ),
      body: Obx(
        () => controller.isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : ListView.builder(
                physics: const BouncingScrollPhysics(),
                itemCount: controller.airsoftList.length,
                itemBuilder: (context, index) {
                  final dataIndex = controller.airsoftList[index];

                  return controller.airsoftList.isEmpty
                      ? const Center(child: CircularProgressIndicator())
                      : InkWell(
                          onTap: () => Get.toNamed('/edit', arguments: {
                            'id_input': dataIndex.id,
                            'name_input': dataIndex.name,
                            'price_input': dataIndex.price.toString(),
                            'desc_input': dataIndex.description,
                            'file_input':
                                '${Global.imageBaseUrl}/${dataIndex.photo}',
                          }),
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Row(
                              children: [
                                ClipRRect(
                                  child: Image(
                                    width: 100.0,
                                    height: 100.0,
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                      '${Global.imageBaseUrl}/${dataIndex.photo}',
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(dataIndex.name.toString()),
                                        const SizedBox(height: 10.0),
                                        Text(dataIndex.price.toString()),
                                        const SizedBox(height: 10.0),
                                        Text(
                                          dataIndex.description.toString(),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                IconButton(
                                    onPressed: () => controller.deleteAirsoft(
                                        dataIndex, dataIndex.id),
                                    icon: const Icon(Icons.delete))
                              ],
                            ),
                          ),
                        );
                },
              ),
      ),
    );
  }
}
