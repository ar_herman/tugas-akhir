import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'edit_controll.dart';

class Editscreen extends StatelessWidget {
  const Editscreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Editcontroller controller = Get.find<Editcontroller>();

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(title: const Text("Update Airsoft")),
        body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Obx(
              () => Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    controller: controller.nameInputController,
                    decoration: const InputDecoration(
                      labelText: 'Name',
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  TextField(
                    controller: controller.priceInputController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(labelText: 'Price'),
                  ),
                  const SizedBox(height: 30.0),
                  TextField(
                    controller: controller.descriptionInputController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(labelText: 'Description'),
                  ),
                  const SizedBox(height: 30.0),
                  const Text(
                    'Photo',
                    style: TextStyle(color: Colors.white60, fontSize: 16.0),
                  ),
                  const SizedBox(height: 30.0),
                  controller.fileImagePicker.value.existsSync()
                      ? InkWell(
                          onTap: () => controller.getImage(),
                          child: Container(
                            width: double.infinity,
                            height: 150.0,
                            color: Colors.grey,
                            child: Image(
                              fit: BoxFit.cover,
                              image:
                                  FileImage(controller.fileImagePicker.value),
                            ),
                          ),
                        )
                      : InkWell(
                          onTap: () => controller.getImage(),
                          child: Container(
                            width: double.infinity,
                            height: 150.0,
                            color: Colors.grey,
                            child: Image(
                              fit: BoxFit.cover,
                              image: NetworkImage('$controller.uploadImageUrl'),
                            ),
                          ),
                        ),
                  const SizedBox(height: 60.0),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        controller.updateAirsoftData(
                          name: controller.nameInputController.text,
                          price: controller.priceInputController.text,
                          description:
                              controller.descriptionInputController.text,
                        );
                      },
                      style: ElevatedButton.styleFrom(primary: Colors.orange),
                      child: const Text('Update'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
