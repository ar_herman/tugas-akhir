import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'add_controll.dart';

class Addscreen extends StatelessWidget {
  const Addscreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Addcontroller controller = Get.find<Addcontroller>();

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(title: const Text("Add Airsoft")),
        body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Obx(
              () => Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    controller: controller.nameInputController,
                    decoration: const InputDecoration(
                      labelText: 'Name',
                    ),
                  ),
                  const SizedBox(height: 30.0),
                  TextField(
                    controller: controller.priceInputController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(labelText: 'Price'),
                  ),
                  const SizedBox(height: 30.0),
                  TextField(
                    controller: controller.descriptionInputController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(labelText: 'Description'),
                  ),
                  const SizedBox(height: 30.0),
                  const Text(
                    'Photo',
                    style: TextStyle(color: Colors.white60, fontSize: 16.0),
                  ),
                  const SizedBox(height: 30.0),
                  controller.fileImagePicker.value.existsSync()
                      ? InkWell(
                          onTap: () => controller.getImage(),
                          child: Container(
                            width: double.infinity,
                            height: 150.0,
                            color: Colors.grey,
                            child: Image(
                              fit: BoxFit.cover,
                              image:
                                  FileImage(controller.fileImagePicker.value),
                            ),
                          ),
                        )
                      : InkWell(
                          onTap: () => controller.getImage(),
                          child: Container(
                            width: double.infinity,
                            height: 150.0,
                            color: Colors.grey,
                            child: const Image(
                                image: AssetImage('assets/no_image.png')),
                          ),
                        ),
                  const SizedBox(height: 60.0),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        controller.postNewAirsoft(
                          name: controller.nameInputController.text,
                          price: controller.priceInputController.text,
                          description:
                              controller.descriptionInputController.text,
                        );
                      },
                      child: const Text('Add'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
