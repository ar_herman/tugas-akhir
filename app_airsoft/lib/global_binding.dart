import 'package:get/get.dart';

import 'ui/home/home_controll.dart';

class GlobalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Homecontroller());
  }
}
